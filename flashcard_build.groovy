pipelineJob('mosar-flashcard-pr') {
    definition {
        environmentVariables {
          env('BUCKET_FLASHCARD', "${BUCKET_FLASHCARD}")
          env('ECS_AGENT_CLUSTER', "${ECS_AGENT_CLUSTER}")
          env('SUBNET_IDS', "${SUBNET_IDS}")
          env('AGENT_SECURITY_GROUP_ID', "${AGENT_SECURITY_GROUP_ID}")
          env('MOSAR_FLASHCARD_REPO', "${MOSAR_FLASHCARD_REPO}")
          env('KANIKO_FLASHCARD_FAMILY', "${KANIKO_FLASHCARD_FAMILY}")

          env('ENVIRONMENT', "${ENVIRONMENT}")
          env('MOSAR_FLASHCARD_REPO', "${MOSAR_FLASHCARD_REPO}")
          env('LOG_GROUP_NAME', "${LOG_GROUP_NAME}")
          env('SPRING_PROFILES_ACTIVE', "${SPRING_PROFILES_ACTIVE}")
          env('LOCK_TABLE_FLASHCARD_TEST', "${LOCK_TABLE_FLASHCARD_TEST}")
          env('LOCK_TABLE_FLASHCARD_PROD', "${LOCK_TABLE_FLASHCARD_PROD}")

          keepBuildVariables(true)
        }
        cpsScm {
            scm {
                git{
                	remote {
                		url('https://gitlab.com/devdiv/flashcardservice.git')
                	}
                    branches('${gitlabsourcebranch}')
                }
            }
            scriptPath('flashcards.jenkinsfile')
        }
        triggers {
            gitlabPush {
                buildOnMergeRequestEvents(true)
                buildOnPushEvents(false)
                enableCiSkip(false)
                setBuildDescription(false)
                rebuildOpenMergeRequest('both')
                excludeBranches('master')
            }
        }
        logRotator {
            numToKeep(3)
            artifactNumToKeep(3)
        }
    }
}

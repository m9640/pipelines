pipelineJob('mosar-game-pr') {
    definition {
        environmentVariables {
          env('BUCKET_GAME', "${BUCKET_GAME}")
          env('ECS_AGENT_CLUSTER', "${ECS_AGENT_CLUSTER}")
          env('SUBNET_IDS', "${SUBNET_IDS}")
          env('AGENT_SECURITY_GROUP_ID', "${AGENT_SECURITY_GROUP_ID}")
          env('MOSAR_GAME_REPO', "${MOSAR_GAME_REPO}")
          env('KANIKO_GAME_FAMILY', "${KANIKO_GAME_FAMILY}")

          env('ENVIRONMENT', "${ENVIRONMENT}")
          env('MOSAR_GAME_REPO', "${MOSAR_GAME_REPO}")
          env('LOG_GROUP_NAME', "${LOG_GROUP_NAME}")
          env('SPRING_PROFILES_ACTIVE', "${SPRING_PROFILES_ACTIVE}")
          env('LOCK_TABLE_GAME_TEST', "${LOCK_TABLE_GAME_TEST}")
          env('LOCK_TABLE_GAME_PROD', "${LOCK_TABLE_GAME_PROD}")


          keepBuildVariables(true)
        }
        cpsScm {
            scm {
                git{
                	remote {
                		url('https://gitlab.com/devdiv/gameservice.git')
                	}
                    branches('${gitlabsourcebranch}')
                }
            }
            scriptPath('game.jenkinsfile')
        }
        triggers {
            gitlabPush {
                buildOnMergeRequestEvents(true)
                buildOnPushEvents(false)
                enableCiSkip(false)
                setBuildDescription(false)
                rebuildOpenMergeRequest('both')
                excludeBranches('master')
            }
        }
        logRotator {
            numToKeep(3)
            artifactNumToKeep(3)
        }
    }
}

pipelineJob('deploy-mosar-frontend') {
    definition {
        environmentVariables {
          env('BUCKET_FRONTEND', "${BUCKET_FRONTEND}")
          env('LOCK_TABLE_FRONTEND_TEST', "${LOCK_TABLE_FRONTEND_TEST}")
          env('LOCK_TABLE_FRONTEND_PROD', "${LOCK_TABLE_FRONTEND_PROD}")
          env('ENVIRONMENT', "${ENVIRONMENT}")

          keepBuildVariables(true)
        }
        cpsScm {
            scm {
                git{
                	remote {
                		url('https://gitlab.com/devdiv/frontend.git')
                		credentials('gitlab-credentials')
                	}
                    branches('main')
                }
            }
            scriptPath('deploy/frontend.jenkinsfile')
        }
        triggers {
            gitlabPush {
                buildOnMergeRequestEvents(false)
                buildOnPushEvents(true)
                enableCiSkip(false)
                setBuildDescription(false)
                rebuildOpenMergeRequest('never')
                targetBranchRegex('.*main.*')
                // excludeBranches('')
                includeBranches('main')
            }
        }
        logRotator {
            numToKeep(3)
            artifactNumToKeep(3)
        }
    }
}

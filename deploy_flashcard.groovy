pipelineJob('deploy-mosar-flashcard') {
    definition {
        environmentVariables {
          env('BUCKET_FLASHCARD', "${BUCKET_FLASHCARD}")
          env('LOCK_TABLE_FLASHCARD_TEST', "${LOCK_TABLE_FLASHCARD_TEST}")
          env('LOCK_TABLE_FLASHCARD_PROD', "${LOCK_TABLE_FLASHCARD_PROD}")
          env('ENVIRONMENT', "${ENVIRONMENT}")

          keepBuildVariables(true)
        }
        cpsScm {
            scm {
                git{
                	remote {
                		url('https://gitlab.com/devdiv/flashcardservice.git')
                	}
                    branches('main')
                }
            }
            scriptPath('deploy/flashcard.jenkinsfile')
        }
        triggers {
            gitlabPush {
                buildOnMergeRequestEvents(false)
                buildOnPushEvents(true)
                enableCiSkip(false)
                setBuildDescription(false)
                rebuildOpenMergeRequest('never')
                targetBranchRegex('.*main.*')
                excludeBranches('master')
                includeBranches('main')
            }
        }
        logRotator {
            numToKeep(3)
            artifactNumToKeep(3)
        }
    }
}

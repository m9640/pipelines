pipelineJob('deploy-mosar-game') {
    definition {
        environmentVariables {
          env('BUCKET_GAME', "${BUCKET_GAME}")
          env('LOCK_TABLE_GAME_TEST', "${LOCK_TABLE_GAME_TEST}")
          env('LOCK_TABLE_GAME_PROD', "${LOCK_TABLE_GAME_PROD}")
          env('ENVIRONMENT', "${ENVIRONMENT}")

          keepBuildVariables(true)
        }
        cpsScm {
            scm {
                git{
                	remote {
                		url('https://gitlab.com/devdiv/gameservice.git')
                	}
                    branches('main')
                }
            }
            scriptPath('deploy/game.jenkinsfile')
        }
        triggers {
            gitlabPush {
                buildOnMergeRequestEvents(false)
                buildOnPushEvents(true)
                enableCiSkip(false)
                setBuildDescription(false)
                rebuildOpenMergeRequest('never')
                targetBranchRegex('.*main.*')
                excludeBranches('master')
                includeBranches('main')
            }
        }
        logRotator {
            numToKeep(3)
            artifactNumToKeep(3)
        }
    }
}

pipelineJob('mosar-frontend-pr') {
    definition {
        environmentVariables {
          env('BUCKET_FRONTEND', "${BUCKET_FRONTEND}")
          env('ECS_AGENT_CLUSTER', "${ECS_AGENT_CLUSTER}")
          env('SUBNET_IDS', "${SUBNET_IDS}")
          env('AGENT_SECURITY_GROUP_ID', "${AGENT_SECURITY_GROUP_ID}")
          env('MOSAR_FRONTEND_REPO', "${MOSAR_FRONTEND_REPO}")
          env('KANIKO_FRONTEND_FAMILY', "${KANIKO_FRONTEND_FAMILY}")

          env('ENVIRONMENT', "${ENVIRONMENT}")
          env('MOSAR_FRONTEND_REPO', "${MOSAR_FRONTEND_REPO}")
          env('LOG_GROUP_NAME', "${LOG_GROUP_NAME}")
          env('LOCK_TABLE_FRONTEND_TEST', "${LOCK_TABLE_FRONTEND_TEST}")
          env('LOCK_TABLE_FRONTEND_PROD', "${LOCK_TABLE_FRONTEND_PROD}")

          keepBuildVariables(true)
        }
        cpsScm {
            scm {
                git{
                	remote {
                		url('https://gitlab.com/devdiv/frontend.git')
                		// credentials('devidiv-gitlab-connection-frontend')
                	}
                    branches('${gitlabsourcebranch}')
                }
            }
            scriptPath('frontend.jenkinsfile')
        }
        triggers {
            gitlabPush {
                buildOnMergeRequestEvents(true)
                buildOnPushEvents(false)
                enableCiSkip(false)
                setBuildDescription(false)
                rebuildOpenMergeRequest('both')
                // excludeBranches('main')
            }
        }
        logRotator {
            numToKeep(3)
            artifactNumToKeep(3)
        }
    }
}
